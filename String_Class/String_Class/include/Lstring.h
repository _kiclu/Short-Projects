#ifndef _LSTRING_H
#define _LSTRING_H

#include<vector>
#include<iostream>
#include<stdio.h>

class Str {
public:


	Str() {
		string.clear();
		string.push_back('\0');
	}
	Str(char* initString);
	friend std::ostream &operator<<(std::ostream &os,Str &str) {
		int i = 0;
		while (str.string.at(i) != '\0') {
			os << str.string.at(i);
			i++;
		}
		return os;
	}
	friend std::istream &operator>>(std::istream &is,Str &str);
	friend Str &operator+=(Str &lhs,const Str &rhs);
	friend int strEnd(Str str);

	~Str();
	
private:
	std::vector<char> string;
};


std::ostream &operator<<(std::ostream &os,Str &str);
std::istream &operator>>(std::istream &is,Str &str);
Str &operator+=(Str &lhs,const Str &rhs);

#endif // _LSTRING_H