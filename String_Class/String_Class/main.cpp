#include<iostream>
#include"Lstring.h"
#include<vector>
#include<Windows.h>

namespace getStr {
	int checkBadCommand(char c[]);
	int convertStringCommand(char strCommand[]);
	int checkEnd(char strCommand[]);

}
void strInput(std::vector<Str> &strVect);
void strOutput(std::vector<Str> strVect);
void strOutputAll(std::vector<Str> strVect);
void addStrings(std::vector<Str> &strVect);
void addStringsToPos(std::vector<Str> &strVect);

int main() {
	std::vector<Str> a;
	char strCommand[50]="0";
	int command=0;
	while (command != -1) {
		std::cin >> strCommand;
		command = getStr::convertStringCommand(strCommand);
		switch (command) {
		case -2: std::cout << "Invalid command!" << std::endl; break;
		case -1: std::cout << "Exiting!" << std::endl; break;
		case 0: break;
		case 1: strInput(a); break;
		case 2: strOutput(a); break;
		case 3: strOutputAll(a); break;
		case 4: addStrings(a); break;
		case 5: addStringsToPos(a); break;
		}
	}
	Sleep(1000);
	return 0;
}

void strInput(std::vector<Str> &strVect) {
	int pos;
	Str tmpStr;
	std::cout << "Input the string at position " << strVect.size() << ": ";
	std::cin >> tmpStr;
	strVect.push_back(tmpStr);
}
void strOutput(std::vector<Str> strVect) {
	int pos;
	std::cout << "Input the position of string: ";
	std::cin >> pos;
	std::cout << strVect.at(pos) << std::endl;
}
void strOutput(std::vector<Str> strVect, int pos) {
	std::cout << strVect.at(pos);
}
void strOutputAll(std::vector<Str> strVect) {
	for (int i = 0; i < strVect.size(); i++) {
		std::cout << "String position " << i << " = "<<char(34);
		strOutput(strVect, i);
		std::cout << char(34) << std::endl;
	}
}
void addStrings(std::vector<Str> &strVect) {
	Str tmpStr1;
	Str tmpStr2;
	int pos1;
	int pos2;
	std::cout << "Input first string position: ";
	std::cin >> pos1;
	tmpStr1 = strVect.at(pos1);
	std::cout << "Input second string position: ";
	std::cin >> pos2;
	tmpStr2 = strVect.at(pos2);
	tmpStr1 += tmpStr2;
}
void addStringsToPos(std::vector<Str> &strVect) {

}



int getStr::checkBadCommand(char c[]) {
	int i = 0;
	while (c[i] != '\0') {
		if (c[i] > 57 || c[i] < 48)
			return 1;
		i++;
	}
	return 0;
}
int getStr::checkEnd(char strCommand[]) {
	if (strcmp("quit", strCommand) == 0 || strcmp("end", strCommand) == 0)
		return 1;
	return 0;
}
int getStr::convertStringCommand(char strCommand[]) {
	if (checkEnd(strCommand))
		return -1;
	if (checkBadCommand(strCommand))
		return -2;
	return atoi(strCommand);
}

