#include<iostream>
#include<fstream>
#include<vector>
#include<stdio.h>

#define MAX_SIZE 40                 //Maximum size for labyrinth
#define MAX_FILE_NAME               //Maximum number of characters for input file name
#define LIST_FILE "FileList.txt"    //Labyrinth file name list
#define fileNo 1                    //testing purpose only
using namespace std;

struct File{
    char name[50];
};
struct Node{
    int x,y;
    int p[4]={-1,-1,-1,-1};
    int status=-1;
};

int n;
int rotX[4]={0,1,0,-1};
int rotY[4]={-1,0,1,0};

int ifJunction(int n[][MAX_SIZE],int x,int y);
int ifDeadEnd(int n[][MAX_SIZE],int x,int y);
int ifTurn(int n[][MAX_SIZE],int x,int y);
void input(int lab[][MAX_SIZE],vector<Node> &N);
File fileSelect();
void placeNodes(int lab[][MAX_SIZE],vector<Node> &N);
void debug(int lab[][MAX_SIZE],vector<Node> N);
void connectNode(int lab[][MAX_N],vector<Node> &N);

int main(){
    int lab[MAX_SIZE][MAX_SIZE];
    vector<Node> N;

    input(lab,N);
    placeNodes(lab,N);

    debug(lab,N);

    return 0;
}

void input(int lab[][MAX_SIZE],vector<Node> &N){
    File source=fileSelect();
    Node tempNode;
    ifstream labIn(source.name);
    labIn>>n;
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            labIn>>lab[i][j];
            if(!lab[i][j] && (!i || !j)){
                tempNode.x=j;
                tempNode.y=i;
                tempNode.p.clear();
                tempNode.status=1;
                N.push_back(tempNode);
                lab[i][j]=2;
            }
            if(!lab[i][j] && (i==n-1 || j==n-1)){
                tempNode.x=j;
                tempNode.y=i;
                tempNode.p.clear();
                tempNode.status=2;
                N.push_back(tempNode);
                lab[i][j]=2;
            }
        }
    }
}
File fileSelect(){
    ifstream fileListIn(LIST_FILE);
    File f;
    /*int fileNo;
    cin>>fileNo;*/
    for(int i=0;i<fileNo;i++){
        fileListIn>>f.name;
    }
    return f;
}
void debug(int lab[][MAX_SIZE],vector<Node> N){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            switch(lab[i][j]){
            case 0:
                printf("  ");
                break;
            case 1:
                printf("%c%c",char(219),char(219));
                break;
            case 2:
                printf("%c%c",char(177),char(177));
                break;
            }
        }
        printf("\n");
    }

    for(int i=0;i<N.size();i++){
        cout<<i<<": "<<N.at(i).x<<" "<<N.at(i).y<<endl;
    }
}
int ifJunction(int n[][MAX_SIZE],int x,int y){
    int k=0;
    for(int i=0;i<4;i++){
        k+=(n[y+rotY[i]][x+rotX[i]]==0)?1:0;
    }
    if(k>2)
        return 1;
    return 0;
}
int ifDeadEnd(int n[][MAX_SIZE],int x,int y){
    int k=0;
    for(int i=0;i<4;i++){
        k+=(n[y+rotY[i]][x+rotX[i]]==1)?1:0;
    }
    if(k>2)
        return 1;
    return 0;
}
int ifTurn(int n[][MAX_SIZE],int x,int y){
    if( (n[y+rotY[0]][x+rotX[0]]==1 && n[y+rotY[1]][x+rotX[1]]==1) || (n[y+rotY[1]][x+rotX[1]]==1 && n[y+rotY[2]][x+rotX[2]]==1) || (n[y+rotY[2]][x+rotX[2]]==1 && n[y+rotY[3]][x+rotX[3]]==1) || (n[y+rotY[3]][x+rotX[3]]==1 && n[y+rotY[0]][x+rotX[0]]==1) )
        return 1;
    return 0;
}
void placeNodes(int lab[][MAX_SIZE],vector<Node> &N){
    Node tmpNode;
    for(int i=1;i<n-1;i++){
        for(int j=1;j<n-1;j++){
            if((ifJunction(lab,j,i) || ifDeadEnd(lab,j,i) || ifTurn(lab,j,i)) && !lab[i][j]){
                lab[i][j]=2;

                tmpNode.x=j;
                tmpNode.y=i;
                N.push_back(tmpNode);
            }
        }
    }
}
void connectNode(int lab[][MAX_N],vector<Node> &N,int x,int y,int no){
    for(int i=1;i<n;i++){
        if(lab[y+i][x]==2){
            for(int k=0;k<N.size();k++){
                if(N.at(k).x==x && N.at(k).y==y+i){
                    N.at(k).p[2]=no;
                    N.at(no).p[0]=k;
                    break;
                }
            }
        }
        if(N.at(no).p[0]!=-1)
            break;
    }
}
